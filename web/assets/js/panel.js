/**
 * Created by WaveDev on 26.11.2017.
 */
$( document ).ready(function() {

    var simplemde;

    $(".toggle-menu").click(function() {
        $("body").toggleClass('toggled');
    });

    $(".nav-parent").click(function(){
        var ul = $(this).next().toggleClass('ninja');
    });

    $('.js-datepicker').datepicker({
        format: 'dd-mm-yyyy'
    });

    if($('.markdown-editor').length){
        simplemde = new SimpleMDE({element: $('.markdown-editor')[0]})
    }

    $('#add_new_category').click(function (event) {

    });

    $('#add_new_tag').click(function (event) {

    });

    /**
     * This listener display all elements
     */
    $('.search-by-data').focusin(function() {
        var searchPlace = $($(this).attr('data-search-place'));
        searchPlace.find('.search-elements').removeClass('knight-series').removeClass('ninja').addClass('knight-series');
    });

    // /**
    //  * This listener display only checked elements
    //  */
    // $('.search-by-data').focusout(function() {
    //     var obj = $(this);
    //     setTimeout(function(){
    //         var searchPlace = $(obj.attr('data-search-place'));
    //         var elements = searchPlace.find('.search-elements');
    //         elements.removeClass('knight-series').removeClass('ninja');
    //         $.each(elements, function (index, element) {
    //             if($(element).find("input[type='checkbox']").prop( "checked" )){
    //                 $(element).addClass('knight-series');
    //             }else{
    //                 $(element).addClass('ninja');
    //             }
    //         });
    //     }, 2000);
    // });

    $('.search-by-data').keyup(function () {
        var searchPlace = $($(this).attr('data-search-place'));

        var phrase = $(this).val();
        searchPlace.find('.search-elements').removeClass('knight-series').removeClass('ninja').addClass('ninja');
        var elements = searchPlace.find("div[data-name*='"+phrase+"']");
        $.each(elements, function (index, element) {
            if($(element).hasClass('ninja')){
                $(element).removeClass('ninja');
                $(element).addClass('knight-series')
            }
        })
    })
    

});
