/**
 * Created by WaveDev on 03.11.2017.
 */
$( document ).ready(function() {

    var loader = $('<div class="loader"><div class="osahanloading"></div></div>');
    var isInMiddleOfRequest = false;

    /**
     * Obsługa formularza do dodawania komentarzy
     */
    $('.add-comment .comment-form').submit(function (event) {
        event.preventDefault();

        if(!isInMiddleOfRequest){

            var form = $(this);
            var action = form.attr('action');
            var formData = form.serialize();

            $('.add-comment .help-block').hide().parent().removeClass('has-error');
            form.append(loader);

            $.ajax({
                url: action,
                method: 'POST',
                data: formData,
                dataType: "json",
                error: function(data, status, jqXHR) {

                    data = data.responseJSON ? data.responseJSON.children : [];

                    $.each( data, function( index, value ){
                        if (value.hasOwnProperty('errors')) {
                            var element = $('#comment_'+index);
                            element.parent().addClass('has-error');
                            element.next().show().text(value.errors[0]);
                        }
                    });
                },
                success: function(data, status, jqXHR) {
                    $('#comment_content').val('');
                    $('#comment_userName').val('');
                    $('#comment_userEmail').val('');
                    $('#comment_userPage').val('');
                    alert(data.message);
                },
                complete: function () {
                    loader.remove();
                    isInMiddleOfRequest = false;
                }
            });
        }
        isInMiddleOfRequest = true
    });

    $(".toggle-menu").click(function() {
        var body = $("body");

        switch (true){
            case body.hasClass('toggled'):
                body.removeClass('toggled').addClass('toggled-min');
                break;
            case body.hasClass('toggled-min'):
                body.removeClass('toggled-min');
                break;
            default:
                body.addClass('toggled');
        }
    });

});
