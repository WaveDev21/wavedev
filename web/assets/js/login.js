/**
 * Created by WaveDev on 26.11.2017.
 */
$( document ).ready(function() {

    $("#login-form").hide();
    $("#login-form").fadeIn(1000);

    function blinker(){
        $('#blinking').fadeOut("slow");
        $('#blinking').fadeIn("slow");
    }
    setInterval(blinker, 1000);

});
