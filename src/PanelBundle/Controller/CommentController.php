<?php

namespace PanelBundle\Controller;

use AppBundle\Repository\CommentRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class CommentController extends Controller
{

    /**
     * @var CommentRepositoryInterface
     */
    protected $repository;

    public function __construct(CommentRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Action used for returning list of articles
     * @return Response
     */
    public function latestAction() : Response
    {
        return $this->render(
            'PanelBundle:comment:list.html.twig', [
                'comments' => $this->repository->getLatest(),
                'title' => 'comments.latest'
        ]);
    }

}
