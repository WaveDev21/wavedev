<?php

namespace PanelBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;
use AppBundle\Repository\CategoryRepository;
use AppBundle\Repository\GroupRepositoryInterface;
use AppBundle\Service\CategoryService;
use AppBundle\Service\PaginateInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends Controller
{
    /**
     * @var PaginateInterface
     */
    protected $paginateService;

    /**
     * @var GroupRepositoryInterface
     */
    protected $repository;

    public function __construct(PaginateInterface $paginate, GroupRepositoryInterface $repository)
    {
        $this->paginateService = $paginate;
        $this->repository = $repository;
    }

    /**
     * Action returning view with list of articles.
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, int $id = 0)
    {
        $page = $request->get('page', 1);

        return $this->render('PanelBundle:category:index.html.twig', [
            'paginator' => $this->paginateService->getPaginatorPyParams([
                'page' => $page,
                'limit' => $this->getParameter('panel.paginator.limit'),
                'phrase' => $request->get('phrase', '')
            ]),
            'edited' => $this->repository->find($id),
        ]);
    }

    /**
     * Action returning category edit view
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id)
    {
        $category = $this->repository->find($id);
        if(!$category){
            throw new NotFoundHttpException("Category does not exist.");
        }

        $form = $this->createForm(CategoryType::class, $category);
        $form->get('id')->setData($id);

        return $this->render('PanelBundle:category:edit.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * Action adding new category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction()
    {
        $form = $this->createForm(CategoryType::class, new Category());

        return $this->render('PanelBundle:category:edit.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * Action for saveing articles
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveAction(Request $request)
    {
        $categoryData = $request->request->get('category', null);
        $id = (int) ($categoryData!== null && isset($categoryData['id'])) ? $categoryData['id'] : null;
        /** @var Article $article */
        $category = $this->repository->find($id) ? : new Category();

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->repository->save($category);
            return $this->redirectToRoute('panel.category.list', ['id' => $category->getId()]);
        }

        return $this->render('PanelBundle:category:edit.html.twig',[
            'form' => $form->createView()
        ]);
    }

}
