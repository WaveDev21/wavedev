<?php

namespace PanelBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Form\ArticleType;
use AppBundle\Repository\ArticleRepositoryInterface;
use AppBundle\Service\PaginateInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class ArticleController extends Controller
{

    /**
     * @var PaginateInterface
     */
    protected $paginateService;

    /**
     * @var ArticleRepositoryInterface
     */
    protected $repository;

    public function __construct(PaginateInterface $paginate, ArticleRepositoryInterface $repository)
    {
        $this->paginateService = $paginate;
        $this->repository = $repository;
    }

    /**
     * Action returning view with list of articles.
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, int $id = 0)
    {
        return $this->render('PanelBundle:article:index.html.twig', [
            'paginator' => $this->paginateService->getPaginatorPyParams([
                'page' => $request->get('page', 1),
                'phrase' => $request->get('phrase', '')
            ]),
            'edited' => $this->repository->get($id),
        ]);
    }

    /**
     * Action returning view with list of articles.
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function notPublishedAction()
    {
        return $this->render('PanelBundle:article:list.html.twig', [
            'articles' => $this->repository->getNotPublished(),
            'title' => 'not.published.articles'
        ]);
    }

    /**
     * Action returning article edit view
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id)
    {
        $article = $this->repository->find($id);
        if(!$article){
            throw new NotFoundHttpException("Article does not exist.");
        }

        /** @var FormInterface $form */
        $form = $this->createForm(ArticleType::class, $article);

        $form->get('id')->setData($id);

        return $this->render('PanelBundle:article:edit.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * Action adding new article
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction()
    {
        $form = $this->createForm(ArticleType::class, new Article());

        return $this->render('PanelBundle:article:edit.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * Action for saveing articles
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveAction(Request $request)
    {
        $articleData = $request->request->get('article', null);

        $id = (int) ($articleData!== null && isset($articleData['id'])) ? $articleData['id'] : null;
        $article = $this->repository->find($id) ? : new Article();

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->repository->save($article, $this->getUser());
            return $this->redirectToRoute('panel.article.list', ['id' => $article->getId()]);
        }

        return $this->render('PanelBundle:article:edit.html.twig',[
            'form' => $form->createView()
        ]);
    }


}
