<?php

namespace PanelBundle\Controller;

use AppBundle\Entity\Tag;
use AppBundle\Form\TagType;
use AppBundle\Repository\GroupRepositoryInterface;
use AppBundle\Repository\TagRepository;
use AppBundle\Service\PaginateInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TagController extends Controller
{
    /**
     * @var PaginateInterface
     */
    protected $paginateService;

    /**
     * @var GroupRepositoryInterface
     */
    protected $repository;

    public function __construct(PaginateInterface $paginate, GroupRepositoryInterface $repository)
    {
        $this->paginateService = $paginate;
        $this->repository = $repository;
    }

    /**
     * Action returning view with list of tags.
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, int $id = 0)
    {
        $page = $request->get('page', 1);

        return $this->render('PanelBundle:tag:index.html.twig', [
            'paginator' => $this->paginateService->getPaginatorPyParams([
                'page' => $page,
                'limit' => $this->getParameter('panel.paginator.limit'),
                'phrase' => $request->get('phrase', '')
            ]),
            'edited' => $this->repository->find($id),
        ]);
    }

    /**
     * Action returning tag edit view
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id)
    {
        $tag = $this->repository->find($id);
        if(!$tag){
            throw new NotFoundHttpException("Tag does not exist.");
        }

        $form = $this->createForm(TagType::class, $tag);
        $form->get('id')->setData($id);

        return $this->render('PanelBundle:tag:edit.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * Action adding new tag
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction()
    {
        $form = $this->createForm(TagType::class, new Tag());

        return $this->render('PanelBundle:tag:edit.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * Action for saveing tags
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveAction(Request $request)
    {
        $tagData = $request->request->get('tag', null);
        $id = (int) ($tagData!== null && isset($tagData['id'])) ? $tagData['id'] : null;
        /** @var Tag $tag */
        $tag = $this->repository->find($id) ? : new Tag();

        $form = $this->createForm(TagType::class, $tag);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->repository->save($tag);
            return $this->redirectToRoute('panel.tag.list', ['id' => $tag->getId()]);
        }

        return $this->render('PanelBundle:tag:edit.html.twig',[
            'form' => $form->createView()
        ]);
    }

}
