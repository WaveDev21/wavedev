<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 01.11.2017
 * Time: 19:47
 */

namespace ApiBundle\Controller\Base;


use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\Serializer;

class RestController extends FOSRestController
{
    /**
     * @param $data
     * @return mixed|string
     */
    protected function parseToJson($data){
        /** @var Serializer $serializer */
        $serializer = $this->get('jms_serializer');
        return $serializer->serialize($data, 'json');
    }
}