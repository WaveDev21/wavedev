<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 01.11.2017
 * Time: 20:04
 */

namespace ApiBundle\Controller;


use ApiBundle\Controller\Base\RestController;
use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;
use AppBundle\Repository\ArticleRepository;
use AppBundle\Repository\ArticleRepositoryInterface;
use AppBundle\Repository\CommentRepositoryInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CommentController extends RestController
{

    /**
     * @var CommentRepositoryInterface
     */
    protected $repository;

    /**
     * @var ArticleRepositoryInterface
     */
    protected $articleRepository;

    /**
     * @var FormInterface
     */
    protected $form;

    public function __construct(FormInterface $form, CommentRepositoryInterface $repository, ArticleRepositoryInterface $articleRepository)
    {
        $this->form = $form;
        $this->repository = $repository;
        $this->articleRepository = $articleRepository;
    }

    /**
     * Action user for adding new comment. In response returns Errors or message.
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request)
    {
        $this->form->handleRequest($request);
        if($this->form->isSubmitted() && $this->form->isValid())
        {
            $article = $this->articleRepository->find($this->form->get('article_id')->getData());
            $comment = $this->form->getData();
            $comment->setArticle($article);

            $this->repository->save($comment);
            return new Response($this->parseToJson(['message' => $this->get('translator')->trans("comment.added")]), Response::HTTP_OK);
        }
        return new Response($this->parseToJson($this->form), Response::HTTP_BAD_REQUEST);
    }
}