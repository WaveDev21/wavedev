<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 01.11.2017
 * Time: 20:04
 */

namespace ApiBundle\Controller;


use ApiBundle\Controller\Base\RestController;
use AppBundle\Entity\Category;
use AppBundle\Entity\Comment;
use AppBundle\Form\CategoryType;
use AppBundle\Form\CommentType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends RestController
{

    protected $container;
    /**
     * Action user for adding new comment. In response returns Errors or Comment json object.
     * @param Request $request
     * @param int $articleId
     * @return Response
     */
    public function addAction(Request $request, int $articleId)
    {
        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);
        $form->get('slug');

        if($form->isSubmitted() && $form->isValid())
        {
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();

            $comment = $form->getData();
            $article = $em->getReference('AppBundle:Article', $articleId);
            $comment->setArticle($article);

            $em->persist($comment);
            $em->flush();
            return new Response($this->parseToJson($comment), Response::HTTP_OK);
        }
        return new Response($this->parseToJson($form), Response::HTTP_BAD_REQUEST);
    }
}