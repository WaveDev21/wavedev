<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 27.01.2018
 * Time: 10:35
 */

namespace AppBundle\Service;


use AppBundle\Repository\EntityQueryInterface;
use AppBundle\Repository\PaginatorQueryInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;

interface PaginateInterface
{
    /**
     * This method should return paginated items
     * @param array $params
     * @return PaginationInterface
     */
    public function getPaginatorPyParams($params = []);
}