<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 29.10.2017
 * Time: 08:41
 */

namespace AppBundle\Service;

use AppBundle\Repository\PaginatorQueryInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class PaginateService implements PaginateInterface
{
    /**
     * @var PaginatorInterface
     */
    protected $paginator;

    /**
     * @var PaginatorQueryInterface
     */
    protected $paginateRepository;

    /**
     * @var int
     */
    protected $limit = 10;

    public function __construct(PaginatorInterface $paginator, PaginatorQueryInterface $paginateRepository, int $limit)
    {
        $this->paginator = $paginator;
        $this->paginateRepository = $paginateRepository;
        $this->limit = $limit;
    }

    /**
     * This method should return paginated items
     * @param array $params
     * @return PaginationInterface
     */
    public function getPaginatorPyParams( $params = [])
    {
        return $this->paginator->paginate(
            $this->paginateRepository->getEntityListQuery($params),
            $params['page'],
            $this->limit
        );
    }
}