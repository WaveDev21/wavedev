<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 29.10.2017
 * Time: 16:30
 */

namespace AppBundle\Form;


use AppBundle\Entity\Article;
use AppBundle\Entity\Category;
use AppBundle\Entity\Tag;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type as CoreType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('POST')
            ->add('id', CoreType\HiddenType::class, [
                'mapped' => false
            ])
            ->add('title', CoreType\TextType::class, [
                'label' => 'title'
            ])
            ->add('slug', CoreType\TextType::class, [
                'label' => 'slug'
            ])
            ->add('tags', EntityType::class, [
                'class'   => Tag::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.name', 'ASC');
                },
                'label' => 'tags',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('categories', EntityType::class, [
                'class'   => Category::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
                'label' => 'categories',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('content', CoreType\TextareaType::class, [
                'label' => 'content'
            ])
            ->add('publishDate', CoreType\DateType::class, [
                'label' => 'publish.date',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy'
            ])
            ->add('sliceOn', CoreType\IntegerType::class, [
                'label' => 'slice.on'
            ])
            ->add('isPublished', CoreType\CheckboxType::class, [
                'label' => 'is.published',
                'required' => false,
            ]);
    }

}