<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 29.10.2017
 * Time: 16:30
 */

namespace AppBundle\Form;


use AppBundle\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type as CoreType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('POST')
            ->add('article_id', CoreType\HiddenType::class,[
                'mapped' => false
            ])
            ->add('content', CoreType\TextareaType::class, [
                'label' => 'Comment'
            ])
            ->add('userName', CoreType\TextType::class, [
                'label' => 'Sign'
            ])
            ->add('userEmail', CoreType\EmailType::class, [
                'label' => 'Email'
            ])
            ->add('userPage', CoreType\UrlType::class, [
                'label' => 'You\'re page',
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class
        ]);
    }

}