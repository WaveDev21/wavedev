<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 06.03.2018
 * Time: 19:30
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Category
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RedirectRepository")
 * @ORM\Table(name="t_redirect")
 */
class Redirect
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $from = '';

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $to = '';

    /**
     * @ORM\Column(type="integer")
     * @var string
     */
    private $code = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     * @return Redirect
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @param string $to
     * @return Redirect
     */
    public function setTo(string $to): Redirect
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Redirect
     */
    public function setCode(string $code): Redirect
    {
        $this->code = $code;
        return $this;
    }


}