<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 24.10.2017
 * Time: 20:47
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Article
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectRepository")
 * @ORM\Table(name="t_project")
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min = 5,
     *     max = 255,
     *     minMessage="title.least.5.chars",
     *     maxMessage="title.max.255.chars"
     * )
     *
     * @ORM\Column(type="string")
     * @var string
     */
    protected $name = '';

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="text")
     * @var string
     */
    protected $content = '';

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $progress = 0;

    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", unique=true)
     * @var string
     */
    protected $slug = '';

    /**
     * Many Articles has One author
     * @ORM\ManyToOne(targetEntity="User", inversedBy="articles")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     * @var User
     */
    protected $author;

    /**
     * @Assert\NotBlank()
     * @Assert\Date()
     *
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $publishDate;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $addDate;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $updateDate;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @var boolean
     */
    protected $isPublished = false;

    public function __construct()
    {
        $this->addDate = new \DateTime();
        $this->updateDate = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Project
     */
    public function setName(string $name): Project
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Article
     */
    public function setContent($content) : self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent() : string
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getProgress(): int
    {
        return $this->progress;
    }

    /**
     * @param int $progress
     * @return Project
     */
    public function setProgress(int $progress): Project
    {
        $this->progress = $progress;
        return $this;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Article
     */
    public function setSlug($slug) : self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug() : string
    {
        return $this->slug;
    }

    /**
     * Set publishDate
     *
     * @param \DateTime $publishDate
     * @return Article
     */
    public function setPublishDate($publishDate) : self
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    /**
     * Get publishDate
     *
     * @return \DateTime 
     */
    public function getPublishDate() : ?\DateTime
    {
        return $this->publishDate;
    }

    /**
     * Set addDate
     *
     * @param \DateTime $addDate
     * @return Article
     */
    public function setAddDate($addDate) : self
    {
        $this->addDate = $addDate;

        return $this;
    }

    /**
     * Get addDate
     *
     * @return \DateTime 
     */
    public function getAddDate() : \DateTime
    {
        return $this->addDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Article
     */
    public function setUpdateDate($updateDate) : self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate() : \DateTime
    {
        return $this->updateDate;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     * @return Article
     */
    public function setAuthor(User $author = null) : self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return User
     */
    public function getAuthor() : User
    {
        return $this->author;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->isPublished;
    }

    /**
     * @param bool $isPublished
     * @return Project
     */
    public function setIsPublished(bool $isPublished): Project
    {
        $this->isPublished = $isPublished;
        return $this;
    }

}
