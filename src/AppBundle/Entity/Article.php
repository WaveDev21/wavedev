<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 24.10.2017
 * Time: 20:47
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Article
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArticleRepository")
 * @ORM\Table(name="t_article")
 */
class Article
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min = 5,
     *     max = 255,
     *     minMessage="title.least.5.chars",
     *     maxMessage="title.max.255.chars"
     * )
     *
     * @ORM\Column(type="string")
     * @var string
     */
    protected $title = '';

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="text")
     * @var string
     */
    protected $content = '';

    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", unique=true)
     * @var string
     */
    protected $slug = '';

    /**
     * Many Articles has One author
     * @ORM\ManyToOne(targetEntity="User", inversedBy="articles")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     * @var User
     */
    protected $author;

    /**
     * One user has many posts
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="article")
     * @var Collection
     */
    protected $comments;

    /**
     * @Assert\NotBlank()
     * @Assert\Date()
     *
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $publishDate;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $addDate;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $updateDate;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="integer",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\GreaterThan(
     *     value = 0
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     * @var int
     */
    protected $sliceOn = 300;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @var boolean
     */
    protected $isPublished = false;

    /**
     * @ORM\Column(type="integer", nullable=true, unique=true)
     * @var int
     */
    protected $publishOrder;

    /**
     * Many Articles has Many categories
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="articles")
     * @ORM\JoinTable(name="t_articles_categories")
     * @var Collection
     */
    protected $categories;

    /**
     * Many Articles has many tags
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="articles")
     * @ORM\JoinTable(name="t_articles_tags")
     * @var Collection
     */
    protected $tags;


    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->addDate = new \DateTime();
        $this->updateDate = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Article
     */
    public function setTitle($title) : self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Article
     */
    public function setContent($content) : self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent() : string
    {
        return $this->content;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Article
     */
    public function setSlug($slug) : self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug() : string
    {
        return $this->slug;
    }

    /**
     * Set publishDate
     *
     * @param \DateTime $publishDate
     * @return Article
     */
    public function setPublishDate($publishDate) : self
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    /**
     * Get publishDate
     *
     * @return \DateTime 
     */
    public function getPublishDate() : ?\DateTime
    {
        return $this->publishDate;
    }

    /**
     * Set addDate
     *
     * @param \DateTime $addDate
     * @return Article
     */
    public function setAddDate($addDate) : self
    {
        $this->addDate = $addDate;

        return $this;
    }

    /**
     * Get addDate
     *
     * @return \DateTime 
     */
    public function getAddDate() : \DateTime
    {
        return $this->addDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Article
     */
    public function setUpdateDate($updateDate) : self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate() : \DateTime
    {
        return $this->updateDate;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     * @return Article
     */
    public function setAuthor(User $author = null) : self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return User
     */
    public function getAuthor() : User
    {
        return $this->author;
    }

    /**
     * Add comments
     *
     * @param Comment $comments
     * @return Article
     */
    public function addComment(Comment $comments) : self
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param Comment $comments
     */
    public function removeComment(Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments() : Collection
    {
        return $this->comments;
    }

    /**
     * Set sliceOn
     *
     * @param int $sliceOn
     * @return Article
     */
    public function setSliceOn(int $sliceOn) : self
    {
        $this->sliceOn = $sliceOn;

        return $this;
    }

    /**
     * Get sliceOn
     *
     * @return \int 
     */
    public function getSliceOn() : int
    {
        return $this->sliceOn;
    }

    /**
     * Add categories
     *
     * @param Category $category
     * @return Article
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param Category $category
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add tags
     *
     * @param Tag $tag
     * @return Article
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->isPublished;
    }

    /**
     * @param bool $isPublished
     * @return Article
     */
    public function setIsPublished(bool $isPublished): Article
    {
        $this->isPublished = $isPublished;
        return $this;
    }

    /**
     * @return int
     */
    public function getPublishOrder() : int
    {
        return $this->publishOrder;
    }

    /**
     * @param mixed $publishOrder
     * @return Article
     */
    public function setPublishOrder($publishOrder) : self
    {
        $this->publishOrder = $publishOrder;
        return $this;
    }

}
