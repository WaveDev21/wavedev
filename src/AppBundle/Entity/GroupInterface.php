<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 06.03.2018
 * Time: 13:32
 */

namespace AppBundle\Entity;


use Doctrine\Common\Collections\Collection;

interface GroupInterface
{

    /**
     * Get id
     *
     * @return integer
     */
    public function getId();


    /**
     * Set content
     *
     * @param $name
     * @return GroupInterface
     */
    public function setName($name);

    /**
     * Get content
     *
     * @return string
     */
    public function getName();

    /**
     * Set addDate
     *
     * @param \DateTime $addDate
     * @return GroupInterface
     */
    public function setAddDate($addDate);

    /**
     * Get addDate
     *
     * @return \DateTime
     */
    public function getAddDate();

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return GroupInterface
     */
    public function setUpdateDate($updateDate);

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate();


    /**
     * Add articles
     *
     * @param Article $articles
     * @return GroupInterface
     */
    public function addArticle(Article $articles);

    /**
     * Remove articles
     *
     * @param Article $articles
     */
    public function removeArticle(Article $articles);

    /**
     * Get articles
     *
     * @return Collection
     */
    public function getArticles();

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return GroupInterface
     */
    public function setSlug($slug);

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug();

    /**
     * @return string
     */
    public function __toString();

}