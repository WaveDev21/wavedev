<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 24.10.2017
 * Time: 20:47
 */

namespace AppBundle\Entity;

use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Comment
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommentRepository")
 * @ORM\Table(name="t_comment")
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * Many Comments has one author
     *
     * @MaxDepth(1)
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="comments")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     * @var User
     */
    private $author;

    /**
     * Many comments has one article
     *
     * @MaxDepth(1)
     * @Exclude(if="true")
     *
     *
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="comments")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id", nullable=false)
     * @var Article
     */
    private $article;


    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="text")
     * @var string
     */
    private $content = '';

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="2")
     *
     * @ORM\Column(type="string")
     * @var string
     */
    private $userName = '';

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     *
     * @ORM\Column(type="string")
     * @var string
     */
    private $userEmail = '';

    /**
     * @Assert\Url()
     *
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $userPage = '';

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $addDate;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updateDate;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @var boolean
     */
    protected $isPublished = false;

    public function __construct()
    {
        $this->addDate = new \DateTime();
        $this->updateDate = new \DateTime();
        $this->isPublished = false;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Comment
     */
    public function setContent($content) : self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent() : string
    {
        return $this->content;
    }

    /**
     * Set addDate
     *
     * @param \DateTime $addDate
     * @return Comment
     */
    public function setAddDate($addDate) : self
    {
        $this->addDate = $addDate;

        return $this;
    }

    /**
     * Get addDate
     *
     * @return \DateTime 
     */
    public function getAddDate() : \DateTime
    {
        return $this->addDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Comment
     */
    public function setUpdateDate($updateDate) : self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate() : \DateTime
    {
        return $this->updateDate;
    }

    /**
     * Set author
     *
     * @param User $author
     * @return Comment
     */
    public function setAuthor(User $author = null) : self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return User
     */
    public function getAuthor() : User
    {
        return $this->author;
    }

    /**
     * Set article
     *
     * @param Article $article
     * @return Comment
     */
    public function setArticle(Article $article = null) : self
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return Article
     */
    public function getArticle() : Article
    {
        return $this->article;
    }

    /**
     * Set userName
     *
     * @param string $userName
     * @return Comment
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string 
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set userEmail
     *
     * @param string $userEmail
     * @return Comment
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * Get userEmail
     *
     * @return string 
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * Set userPage
     *
     * @param string $userPage
     * @return Comment
     */
    public function setUserPage($userPage)
    {
        $this->userPage = $userPage;

        return $this;
    }

    /**
     * Get userPage
     *
     * @return string 
     */
    public function getUserPage()
    {
        return $this->userPage;
    }
}
