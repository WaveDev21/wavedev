<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 24.10.2017
 * Time: 20:47
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tag
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TagRepository")
 * @ORM\Table(name="t_tag")
 */
class Tag implements GroupInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name = '';

    /**
     * @ORM\Column(type="string", unique=true)
     * @var string
     */
    private $slug = '';

    /**
     * Many tags has many articles
     * @ORM\ManyToMany(targetEntity="Article", mappedBy="tags")
     * @var Collection
     */
    private $articles;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $addDate;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updateDate;

    public function __construct()
    {
        $this->addDate = new \DateTime();
        $this->updateDate = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() : int
    {
        return $this->id;
    }


    /**
     * Set content
     *
     * @param $name
     * @return Tag
     */
    public function setName($name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * Set addDate
     *
     * @param \DateTime $addDate
     * @return Tag
     */
    public function setAddDate($addDate) : self
    {
        $this->addDate = $addDate;

        return $this;
    }

    /**
     * Get addDate
     *
     * @return \DateTime 
     */
    public function getAddDate() : \DateTime
    {
        return $this->addDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Tag
     */
    public function setUpdateDate($updateDate) : self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate() : \DateTime
    {
        return $this->updateDate;
    }


    /**
     * Add articles
     *
     * @param Article $articles
     * @return Tag
     */
    public function addArticle(Article $articles)
    {
        $this->articles[] = $articles;

        return $this;
    }

    /**
     * Remove articles
     *
     * @param Article $articles
     */
    public function removeArticle(Article $articles)
    {
        $this->articles->removeElement($articles);
    }

    /**
     * Get articles
     *
     * @return Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Tag
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return (string) $this->getName();
    }
}
