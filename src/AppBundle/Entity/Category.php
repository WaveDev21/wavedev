<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 24.10.2017
 * Time: 20:47
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Category
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 * @ORM\Table(name="t_category")
 */
class Category implements GroupInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min = 3,
     *     max = 255,
     *     minMessage="name.least.3.chars",
     *     maxMessage="name.max.255.chars"
     * )
     * @ORM\Column(type="string")
     * @var string
     */
    private $name = '';

    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", unique=true)
     * @var string
     */
    private $slug = '';

    /**
     * Many comments has many articles
     * @ORM\ManyToMany(targetEntity="Article", mappedBy="categories")
     * @var Collection
     */
    private $articles;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $addDate;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updateDate;

    public function __construct()
    {
        $this->addDate = new \DateTime();
        $this->updateDate = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() : int
    {
        return $this->id;
    }


    /**
     * Set content
     *
     * @param $name
     * @return Category
     */
    public function setName($name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * Set addDate
     *
     * @param \DateTime $addDate
     * @return Category
     */
    public function setAddDate($addDate) : self
    {
        $this->addDate = $addDate;

        return $this;
    }

    /**
     * Get addDate
     *
     * @return \DateTime 
     */
    public function getAddDate() : \DateTime
    {
        return $this->addDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Category
     */
    public function setUpdateDate($updateDate) : self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate() : \DateTime
    {
        return $this->updateDate;
    }


    /**
     * Add articles
     *
     * @param Article $articles
     * @return Category
     */
    public function addArticle(Article $articles)
    {
        $this->articles[] = $articles;

        return $this;
    }

    /**
     * Remove articles
     *
     * @param Article $articles
     */
    public function removeArticle(Article $articles)
    {
        $this->articles->removeElement($articles);
    }

    /**
     * Get articles
     *
     * @return Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return (string) $this->getName();
    }
}
