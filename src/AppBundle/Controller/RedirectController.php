<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Redirect;
use AppBundle\Repository\ArticleRepository;
use AppBundle\Repository\RedirectRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RedirectController extends Controller
{

    /**
     * @var RedirectRepositoryInterface
     */
    protected $repository;

    public function __construct(RedirectRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * This action should redirect all not matched actions
     * @param $url
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectAction($url)
    {
        /** @var Redirect $redirect */
        $redirect = $this->repository->findOneBy(['from' => $url]);
        if($redirect !== null){
            return $this->redirect($redirect->getTo(), $redirect->getCode());
        }
        throw new NotFoundHttpException();
    }

}
