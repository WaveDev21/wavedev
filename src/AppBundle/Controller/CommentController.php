<?php

namespace AppBundle\Controller;

use AppBundle\Repository\CommentRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;

class CommentController extends Controller
{

    /**
     * @var CommentRepositoryInterface
     */
    protected $repository;

    /**
     * @var FormInterface
     */
    protected $form;

    public function __construct(FormInterface $form, CommentRepositoryInterface $repository)
    {
        $this->form = $form;
        $this->repository = $repository;
    }

    /**
     * Action used for creating Comment new instance.
     * @param $articleId
     * @return Response
     */
    public function addAction(int $articleId)
    {
        $this->form->get('article_id')->setData($articleId);
        return $this->render(
            'AppBundle:comment:add.html.twig',
            array('form' => $this->form->createView())
        );
    }

    /**
     * Action used for returning list of articles
     * @param $articleId
     * @return Response
     */
    public function listAction(int $articleId)
    {
        return $this->render(
            'AppBundle:comment:list.html.twig',
            array('comments' => $this->repository->getAllForArticle($articleId))
        );
    }

}
