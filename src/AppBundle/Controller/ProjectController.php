<?php

namespace AppBundle\Controller;

use AppBundle\Repository\ArticleRepository;
use AppBundle\Repository\ArticleRepositoryInterface;
use AppBundle\Repository\ProjectRepository;
use AppBundle\Service\PaginateInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProjectController extends Controller
{

    /**
     * @var PaginateInterface
     */
    protected $paginateService;

    /**
     * @var ArticleRepositoryInterface
     */
    protected $repository;

    /**
     * ProjectController constructor.
     * @param PaginateInterface $paginate
     * @param ProjectRepository $repository
     */
    public function __construct(PaginateInterface $paginate, ProjectRepository $repository)
    {
        $this->paginateService = $paginate;
        $this->repository = $repository;
    }

    public function articleListAction() : Response
    {
        return new Response('Article list');
    }

    /**
     * Action returning view with list of articles.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request) : Response
    {
        $page = $request->get('page', 1);

        return $this->render('AppBundle:project:index.html.twig', [
            'paginator' => $this->paginateService->getPaginatorPyParams([
                'page' => $page,
                'published' => true,
            ]),
        ]);
    }

}
