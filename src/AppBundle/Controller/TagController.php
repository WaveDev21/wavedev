<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Tag;
use AppBundle\Repository\GroupRepositoryInterface;
use AppBundle\Service\PaginateInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TagController extends Controller
{

    /**
     * @var PaginateInterface
     */
    protected $paginate;

    /**
     * @var GroupRepositoryInterface
     */
    protected $repository;

    public function __construct(PaginateInterface $paginate, GroupRepositoryInterface $repository)
    {
        $this->paginate = $paginate;
        $this->repository = $repository;
    }

    /**
     * Action used for returning list of categories
     * @return Response
     */
    public function listAction()
    {
        return $this->render(
            'AppBundle:tag:list.html.twig',
            array('tags' => $this->repository->getAllPublished())
        );
    }

    /**
     * Action used for returning list of articles related with tag
     * @param Request $request
     * @param $slug
     * @return Response
     */
    public function articlesAction(Request $request, $slug)
    {
        $page = $request->get('page', 1);

        /** @var Tag $tag */
        $tag = $this->repository->findOneBy(['slug'=>$slug]);

        if(!$tag){
            throw new NotFoundHttpException("Tag does not exist.");
        }

        return $this->render('AppBundle:article:index.html.twig', [
            'paginator' => $this->paginate->getPaginatorPyParams([
                'tag' => $tag,
                'published' => true,
                'page' => $page,
            ]),
        ]);
    }

}
