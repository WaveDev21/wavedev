<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Repository\GroupRepositoryInterface;
use AppBundle\Service\PaginateInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends Controller
{

    /**
     * @var PaginateInterface
     */
    protected $paginate;

    /**
     * @var GroupRepositoryInterface
     */
    protected $repository;

    public function __construct(PaginateInterface $paginate, GroupRepositoryInterface $repository)
    {
        $this->paginate = $paginate;
        $this->repository = $repository;
    }

    /**
     * Action used for returning list of categories
     * @return Response
     */
    public function listAction()
    {
        return $this->render(
            'AppBundle:category:list.html.twig',
            array('categories' => $this->repository->getAllPublished())
        );
    }

    /**
     * Action used for returning list of articles related with category
     * @param Request $request
     * @param $slug
     * @return Response
     */
    public function articlesAction(Request $request, $slug)
    {
        $page = $request->get('page', 1);

        /** @var Category $category */
        $category = $this->repository->findOneBy(['slug'=>$slug]);

        if(!$category){
            throw new NotFoundHttpException("Comment does not exist.");
        }

        return $this->render('AppBundle:article:index.html.twig', [
            'paginator' => $this->paginate->getPaginatorPyParams([
                'category' => $category,
                'published' => true,
                'page' => $page,
            ]),
        ]);
    }

}
