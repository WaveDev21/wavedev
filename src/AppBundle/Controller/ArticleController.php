<?php

namespace AppBundle\Controller;

use AppBundle\Repository\ArticleRepository;
use AppBundle\Repository\ArticleRepositoryInterface;
use AppBundle\Service\PaginateInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArticleController extends Controller
{
    /**
     * @var PaginateInterface
     */
    protected $paginateService;

    /**
     * @var ArticleRepositoryInterface
     */
    protected $repository;

    public function __construct(PaginateInterface $paginate, ArticleRepositoryInterface $repository)
    {
        $this->paginateService = $paginate;
        $this->repository = $repository;
    }

    /**
     * Action returning view with list of articles.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $page = $request->get('page', 1);

        return $this->render('AppBundle:article:index.html.twig', [
            'paginator' => $this->paginateService->getPaginatorPyParams([
                'page' => $page,
                'phrase' => $request->get('phrase', ''),
                'published' => true,
            ]),
        ]);
    }

    /**
     * Action returning view with concrete article, witch could be found by id value.
     * routing here is /article/{slug}/{id}
     * slug parameter only for google, not used i any other way for now.
     * @param $id
     * @return mixed
     */
    public function showAction($id)
    {
        $article = $this->repository->get($id);

        if(!$article){
            throw new NotFoundHttpException("Article does not exist.");
        }

        return $this->render('AppBundle:article:show.html.twig',[
            'article' => $article,
            'prev' =>  $this->repository->getPrev($article),
            'next' =>  $this->repository->getNext($article),
        ]);
    }

}
