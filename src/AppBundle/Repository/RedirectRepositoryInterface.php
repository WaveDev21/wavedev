<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 06.03.2018
 * Time: 19:33
 */

namespace AppBundle\Repository;


interface RedirectRepositoryInterface
{
    /**
     * This method should return one element or null
     * @param array $args
     * @return mixed
     */
    public function findOneBy(array $args);

}