<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 13.11.2017
 * Time: 19:58
 */

namespace AppBundle\Repository;

use Doctrine\Common\Collections\Collection;

interface ItemQueryInterface
{
    /**
     * This method return list of items witch should be published
     * @return array|Collection
     */
     public function getPublishedItemList();
}