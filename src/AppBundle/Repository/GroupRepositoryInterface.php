<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 05.03.2018
 * Time: 10:50
 */

namespace AppBundle\Repository;


use AppBundle\Entity\GroupInterface;

interface GroupRepositoryInterface
{
    /**
     * This method return all categories witch published articles.
     * @return mixed
     */
    public function getAllPublished();

    /**
     * This method return all categories connected to passed article
     * @return mixed
     */
    public function getForArticle();

    /**
     * This method saves current state of category object
     * @param $item
     * @return void
     */
    public function save(GroupInterface $item);

    /**
     * This method return one category and search for one of params
     * @param array $args
     * @return mixed
     */
    public function findOneBy(array $args);

    /**
     * @param $id
     * @return mixed
     */
    public function find($id);

}