<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 13.11.2017
 * Time: 19:58
 */

namespace AppBundle\Repository;

use Doctrine\ORM\Query as DoctrineQuery;

interface PaginatorQueryInterface
{
    /**
     * Metoda zrwacająca zapytanie do bazy danych mające za zadanie zwrucenie listy artykułów
     * @param array $params
     * @return DoctrineQuery
     */
     public function getEntityListQuery(array $params);
}