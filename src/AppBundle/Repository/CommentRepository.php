<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Article;
use AppBundle\Entity\Comment;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Symfony\Component\Form\FormInterface;

/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 29.10.2017
 * Time: 08:31
 */
class CommentRepository extends EntityRepository implements CommentRepositoryInterface
{

    public function __construct(EntityManager $em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
    }

    /**
     * This method return list of comments
     * @param int $articleId
     * @return mixed
     */
    public function getAllForArticle(int $articleId)
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('c')
            ->from(Comment::class, 'c')
            ->leftJoin('c.article', 'a')
            ->where('a.id = :articleId')
            ->addOrderBy('c.addDate', 'ASC')
            ->setParameter('articleId', $articleId)
            ->getQuery()
            ->getResult();
    }

    /**
     * This method is responsible for saweing object state
     * @param Comment $comment
     * @return mixed
     */
    public function save(Comment $comment)
    {
        $this->getEntityManager()->persist($comment);
        $this->getEntityManager()->flush();
    }

    /**
     * This method should return array of latest elements
     * @return mixed
     */
    public function getLatest(): array
    {
        $date = new \DateTime();
        $dateInterval = new \DateInterval('P1M');
        $dateInterval->invert = 1;
        $date->add($dateInterval);

        return $this->createQueryBuilder('c')
            ->where('c.addDate > :date')
            ->addOrderBy('c.addDate', 'DESC')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();
    }
}