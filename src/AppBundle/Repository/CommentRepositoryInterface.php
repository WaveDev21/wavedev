<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 06.03.2018
 * Time: 14:25
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Comment;
use Symfony\Component\Form\FormInterface;

interface CommentRepositoryInterface
{
    /**
     * This method return list of comments
     * @param int $articleId
     * @return mixed
     */
    public function getAllForArticle(int $articleId);

    /**
     * This method is responsible for saweing object state
     * @param Comment $comment
     * @return mixed
     */
    public function save(Comment $comment);

    /**
     * This method should return array of latest elements
     * @return mixed
     */
    public function getLatest() : array;
}