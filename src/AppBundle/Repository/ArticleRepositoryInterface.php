<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 05.03.2018
 * Time: 10:50
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Article;
use AppBundle\Entity\User;
use Symfony\Component\Form\FormInterface;

interface ArticleRepositoryInterface
{
    /**
     * This method return single published item or null.
     * @param $id
     * @return mixed
     */
    public function get(int $id);

    /**
     * This method return single published item or null.
     * @param int $id
     * @param null $lockMode
     * @param null $lockVersion
     * @return mixed
     */
    public function find($id, $lockMode = null, $lockVersion = null);

    /**
     * This method should return single item or null
     * @param Article $article
     * @return mixed
     */
    public function getPrev(Article $article);

    /**
     * This method should return single item or null
     * @param Article $article
     * @return mixed
     */
    public function getNext(Article $article);

    /**
     * This method save current state of entity if needed create new.
     * @param Article $form
     * @param User|null $user
     * @return void
     */
    public function save(Article $form, User $user = null);

    /**
     * This method should return Not Published articles
     * @return array
     */
    public function getNotPublished(): array;

}