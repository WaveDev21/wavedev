<?php

namespace AppBundle\Repository;

use AppBundle\Entity\GroupInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\Query as DoctrineQuery;

/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 29.10.2017
 * Time: 08:31
 */
class TagRepository extends EntityRepository implements PaginatorQueryInterface, GroupRepositoryInterface
{

    public function __construct(EntityManager $em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
    }

    /**
     * This method return Doctrine Query instance. Query for all articles in category.
     * @param array $params
     * @return DoctrineQuery
     * @internal param Category $category
     */
    public function getEntityListQuery(array $params) : DoctrineQuery
    {
        $builder = $this->getEntityManager()->createQueryBuilder()
            ->select('t')
            ->from('AppBundle:Tag', 't')
            ->orderBy('t.name', 'ASC');

        if(isset($params['phrase']) && !empty($params['phrase'])){
            $builder->andWhere('t.name LIKE :phrase')
                ->setParameter('phrase', '%'.$params['phrase'].'%');
        }

        return $builder->getQuery();
    }

    /**
     * This method return all categories witch published articles.
     * @return mixed
     */
    public function getAllPublished()
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('t')
            ->from('AppBundle:Tag', 't')
            ->innerJoin('t.articles', 'a', 'WITH', 'a.isPublished = 1')
            ->getQuery()
            ->getResult();
    }

    /**
     * Method used for save articles
     * @param GroupInterface $group
     */
    public function save(GroupInterface $group)
    {
        $group->setUpdateDate(new \DateTime());
        $this->getEntityManager()->persist($group);
        $this->getEntityManager()->flush();
    }

    /**
     * This method return all categories connected to passed article
     * @return mixed
     */
    public function getForArticle()
    {
        // TODO: Implement getForArticle() method.
    }
}