<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Article;
use AppBundle\Entity\Category;
use AppBundle\Entity\Tag;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query as DoctrineQuery;

/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 29.10.2017
 * Time: 08:31
 */
class ArticleRepository extends EntityRepository implements PaginatorQueryInterface, ArticleRepositoryInterface
{

    /**
     * This method return Doctrine Query instance. Query for all articles.
     * @param array $params
     * @return DoctrineQuery
     */
    public function getEntityListQuery(array $params) : DoctrineQuery
    {
        $builder = $this->getEntityManager()->createQueryBuilder()
            ->select('p')
            ->from('AppBundle:Article', 'p')
            ->orderBy('p.publishDate', 'DESC');

        if(isset($params['published']) && !empty($params['published'])){
            $builder->andWhere('p.isPublished = :published')
                ->setParameter('published', $params['published']);
        }

        if(isset($params['phrase']) && !empty($params['phrase'])){
            $builder->andWhere('p.title LIKE :phrase')
                ->setParameter('phrase', '%'.$params['phrase'].'%');
        }

        if(isset($params['tag']) && $params['tag'] instanceof Tag){
            $builder->innerJoin('p.tags', 't', 'WITH', 't = :tag')
                ->setParameter('tag', $params['tag']);
        }

        if(isset($params['category']) && $params['category'] instanceof Category){
            $builder->innerJoin('p.categories', 'c', 'WITH', 'c = :category')
                ->setParameter('category', $params['category']);
        }

        return $builder->getQuery();
    }

    /**
     * Method returning Article instance or NULL
     * @param $id
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function get(int $id) : ?Article
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('a')
            ->from('AppBundle:Article', 'a')
            ->where('a.id = :id')
            ->andWhere('a.isPublished = :isPublished')
            ->setParameters([
                'id' => $id,
                'isPublished' => true
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Method returning previous Article instance or NULL
     * @param Article $article
     * @return Article|mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPrev(Article $article) : ?Article
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('a')
            ->from('AppBundle:Article', 'a')
            ->where('a.publishOrder < :publishOrder')
            ->setParameter('publishOrder', $article->getPublishOrder())
            ->orderBy('a.publishOrder', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Method returning next Article instance or NULL
     * @param Article $article
     * @return Article|mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNext(Article $article) : ?Article
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('a')
            ->from('AppBundle:Article', 'a')
            ->where('a.publishOrder > :publishOrder')
            ->setParameter('publishOrder', $article->getPublishOrder())
            ->orderBy('a.publishOrder', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Method user for getting latest publish order number
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMaxPublishOrder() : int
    {
        return (int) $this->getEntityManager()->createQueryBuilder()
            ->select('MAX(a.publishOrder) as publishOrder')
            ->from('AppBundle:Article', 'a')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Method used for save articles
     * @param Article $article
     * @param User|null $user
     * @return void
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Article $article, User $user = null)
    {
        $article->setAuthor($user);

        if($article->isPublished()){
            $article->setPublishOrder($this->getMaxPublishOrder() + 1);
        }

        $article->setUpdateDate(new \DateTime());
        $this->getEntityManager()->persist($article);
        $this->getEntityManager()->flush();
    }

    /**
     * This method should return Not Published articles
     * @return array
     */
    public function getNotPublished(): array
    {
        return $this->createQueryBuilder('a')
            ->where('a.isPublished = 0')
            ->orderBy('a.publishDate', 'ASC')
            ->getQuery()
            ->getResult();
    }
}