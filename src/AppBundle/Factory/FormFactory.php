<?php
/**
 * Created by PhpStorm.
 * User: WaveDev
 * Date: 06.03.2018
 * Time: 14:20
 */

namespace AppBundle\Factory;


use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;
use Symfony\Component\Form\FormFactoryInterface;

class FormFactory
{
    /**
     * This method returns comment form
     * @param $formFactory
     * @return mixed
     */
    public function createComment(FormFactoryInterface $formFactory)
    {
        return $formFactory->create(CommentType::class, new Comment());
    }

}