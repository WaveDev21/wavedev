<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{

    /**
     * @dataProvider urlProvider
     * @param $path
     */
    public function testPageIsSuccessful($path)
    {
        $client = self::createClient();
        $client->request('GET', $path);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    /**
     * Metoda sprawdzająca czy layout jest wszędzie identyczny
     * @dataProvider urlProvider
     * @param $path
     */
    public function testPageLayout($path)
    {
        $client = static::createClient();
        $crawler = $client->request('GET', $path);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        /** Asserting header exist on page */
        $this->assertCount(1, $crawler->filter('header'));
        /** Asserting on page is only one h1 tag */
        $this->assertCount(1, $crawler->filterXPath('//h1'));
        /** Asserting h1 tag exist with class page-name and has text WaveDev */
        $this->assertContains('WaveDev', $crawler->filter('header h1.page-name')->text());

        /** Asserting topBarSection exist, is section tag and exist inside main tag */
        $this->assertCount(1, $crawler->filterXPath('//header/section[@id="topBarSection"]'));
        /** Asserting search form exist inside top bar */
        $this->assertCount(1, $crawler->filter('#topBarSection form#searchForm'));
        /** Asserting on page is only one search form */
        $this->assertCount(1, $crawler->filter('#searchForm'));
        /** Asserting search form has input with name "phrase" */
        $this->assertCount(1, $crawler->filter('#searchForm input[name="phrase"]'));

        /** Asserting top bar has menu button inside */
        $this->assertCount(1, $crawler->filter('#topBarSection button#menuBtn'));
        /** Asserting on page is only one menu button */
        $this->assertCount(1, $crawler->filter('#menuBtn'));

        /** Asserting footer exist on page */
        $this->assertCount(1, $crawler->filter('footer'));
        /** Asserting footer has additionals section */
        $this->assertCount(1, $crawler->filter('footer section.additionals-section'));
        /** Asserting additionals section has categories and tags container */
        $this->assertCount(2, $crawler->filter('.additionals-section .cloud-container'));
        /** Asserting additionals section has credits container */
        $this->assertCount(1, $crawler->filter('.additionals-section .credits-container'));

    }

    public function urlProvider()
    {
        return [
            ['/'], // app.home
            ['/article/slug/2'] // app.article
        ];
    }

    /**
     * Metoda testująca zawartość strony głównej
     */
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        /** Asserting we got 2 pagination sections */
        $this->assertCount(2, $crawler->filterXPath('//section[contains(@class, "pagination-section")]'));
        /** Asserting pagination has at least 3 a elements */
        $this->assertTrue(3 <= $crawler->filterXPath('//section[contains(@class, "pagination-section")][1]/nav/a')->count());
        /** Asserting both pagination are the same */
        $this->assertEquals($crawler->filterXPath('//section[contains(@class, "pagination-section")][1]')->html(), $crawler->filterXPath('//section[contains(@class, "pagination-section")][2]')->html());

        /** Asserting one list section exist in view */
        $this->assertCount(1, $crawler->filterXPath('//section[contains(@class, "list-section")]'));
    }

    /**
     * Metoda testująca zawartośc strony konkretnego artykułu
     */
    public function testArticle()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/article/slug/2');

        /** Asserting at least one tag article exist in view */
        $this->assertTrue(1 <= $crawler->filterXPath('//article')->count());
        /** Asserting tag article has class full-article*/
        $this->assertTrue(1 == $crawler->filterXPath('//article[contains(@class, "full-article")]')->count());

        /** Asserting in article tag exist tag with class article-title */
        $this->assertCount(1, $crawler->filter('article .article-title'));
        /** Asserting in article tag exist tag with class article-content */
        $this->assertCount(1, $crawler->filter('article .article-content'));
        /** Asserting in article tag exist tag with class add-comment */
        $this->assertCount(1, $crawler->filter('.add-comment'));
        /** Asserting in article tag exist tag with class article-comments */
        $this->assertCount(1, $crawler->filter('.article-comments'));

    }
}
